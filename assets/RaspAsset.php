<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class RaspAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/uikit.css',
        'css/components/notify.min.css',
        'css/header.css',
        'css/footer.css',
        'css/main.css',
    ];
    public $js = [
        'js/uikit.min.js',
        'js/components/notify.min.js',
        'js/main.js',
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

    public $depends = [
        'app\assets\JqueryAsset',
        'app\assets\MomentAsset',
        'app\assets\LodashAsset',
    ];

    // public function init()
    // {
    //     parent::init();
    //     \Yii::$app->assetManager->bundles = [
    //         'yii\\bootstrap\\BootstrapAsset' => [
    //             'css' => [],              
    //         ],
    //         'yii\\bootstrap\\BootstrapPluginAsset' => [
    //             'js' => []                
    //         ]
    //     ];
    // }
}

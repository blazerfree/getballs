<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */


namespace app\commands;

use yii\console\Controller;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;

use app\models\Group;
use app\models\Subject;
use app\models\Lesson;
use app\models\Kms;
use app\models\Prepod;
use app\models\Auditory;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DemoController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    
    public function actionImportLessons()
    {
        $json = file_get_contents(\Yii::$app->basePath . '/data/rasps/2015_2016_2_mp_4.json');
        $rasp = Json::decode($json);

        $bd_groups = Group::find()->all();
        $bd_groups_arr = ArrayHelper::map($bd_groups, 'id', 'faculty', 'num_of_group');

        $bd_subjects = Subject::find()->all();
        $bd_subjects_arr = ArrayHelper::map($bd_subjects, 'name', ['group_id']);

        print_r($bd_groups_arr);
        // // $bd_subjects_names = [];
        
        // foreach ($bd_subjects as $bd_subject) {
        //     if (ArrayHelper::isIn($subject_name, $bd_subject->name)) 
        //         echo $subject_name . "bd_subject_name $bd_subject->name" . " $bd_subject->group_id";
            
        // }

        foreach ($rasp as $index => $lesson) {
            $subject_name = $lesson["name"];
            $subject_group = [
                "faculty" => $lesson["group"]["faculty"],
                "number" => $lesson["group"]["number"],
                "course" => $lesson["group"]["course"],
            ];

            // foreach ($bd_subjects as $bd_subject) {
            //     if (ArrayHelper::isIn($subject_name, $bd_subject->name)) {
            //         echo $subject_name . "bd_subject_name $bd_subject->name" . " $bd_subject->group_id";
            //     }
            // }


            $params = [
                'type_of_lesson' => $lesson["type"],
                'start_pair' => $lesson["pair"],
                'pairs_number' => 1,
                'auditory' => $lesson["auditory"],
                'is_visited' => false,
                // 'subject_id' => $lesson[],
                // 'auditory_id' => $lesson[],
            ];

            // $lesson->createLesson($params);
            // print_r($params);
        }
    }

    public function actionCreateAll($model, $num=10)
    {
    	$faker = \Faker\Factory::create('ru_RU'); 

    	switch ($model) {
    		case 'group':
    			$groups = require(\Yii::$app->basePath . '/data/array_groups.php');

    			foreach ($groups as $thisGroup) {
    				$group = new Group();

    				$group->faculty      = $faker->randomElement($array = array ("MP",));
    				$group->num_of_group = $thisGroup;
    				$group->course       = substr($thisGroup, 0, 1);
    				
    				if (!$group->save()) {
    					print_r($group);
    					break;
    				}
    			}
    			break;
    		case 'subject':
				$groups = Group::find()->all();
				$count_of_groups = Group::find()->count();

				foreach ($groups as $this_group) {
					for ($i=0; $i < 14; $i++) {
	    				$subjects_names = require(\Yii::$app->basePath . '/data/array_subjects.php');
	    				$controls_forms = ["exam","zachet","diff_zachet"];
	    				$ball_limit = $faker->numberBetween($min = 0, $max = 100);

	    				$subject = new Subject();
    				    
    				    $subject->name             = $faker->randomElement($subjects_names);
    				    $subject->date_of_begin    = "2016-09-01";
    				    $subject->date_of_end      = "2016-12-31";
    				    $subject->form_of_control  = $faker->randomElement($controls_forms);
    				    $subject->current_min_ball = $faker->numberBetween($min = 0, $max = $ball_limit);
    				    $subject->current_max_ball = $faker->numberBetween($min = $ball_limit, $max = 100);
    				    $subject->current_ball     = $faker->numberBetween($min = 0, $max = 100);
    				    $subject->group_id         = $this_group->id;
	    				
	    				if (!$subject->save()) {
	    					print_r($subject);
	    					break;
	    				}
					}
				}
    			break;
    		case 'lesson':
    			foreach ($subjects as $this_subject) {
    				$lessons_types = ["seminar","lecture","lab"];

    				$lesson = new Lesson();

    				// $lesson->type_of_lesson = pes);

    				$params = [
			            'type_of_lesson' => '$faker->randomElement($lessons_types)',
			            'start_pair' => 'Start Pair',
			            'pairs_number' => 'Pairs Number',
			            'auditory' => 'Auditory',
			            'is_visited' => 'Is Visited',
			            'subject_id' => 'Subject ID',
			            'auditory_id' => 'Auditory ID',
			        ];

			        $lesson->createLesson($params);

    				if (!$lesson->save()) {
    					print_r($lesson);
    					break;
    				}
    			}
    			break;
    		case 'kms':
    			for ($i=0; $i < $num; $i++)
    			{
    				$kms = new Kms();
    				$kms->save();
    			}
    			break;
    		case 'prepod':
    			for ($i=0; $i < $num; $i++)
    			{
    				$prepod = new Prepod();
    				$prepod->save();
    			}
    			break;
    		case 'auditory':
    			for ($i=0; $i < $num; $i++)
    			{ 
    				$auditory = new Auditory();
    				$num_1 = $faker->numberBetween($min = 1, $max = 4);
    				$num_2 = $faker->numberBetween($min = 1, $max = 3);
    				$num_3 = $faker->numberBetween($min = 10, $max = 50);
    				$auditory->number = "{$num_1}{$num_2}{$num_3}";
    				$auditory->save();
    			}
    		default:
    			echo "\n@params: group, subject, lesson, kms, prepod, auditory";
    			break;
    	}
    }
}

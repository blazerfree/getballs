<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;


$this->title = 'Расписание МИЭТ';
?>

<div class="uk-grid">
	<div class="uk-width-3-3">
		<ul class="uk-subnav uk-subnav-pill">
			<?php foreach ($groups as $group): ?>
			    <li class="<?=($group['id'] == $this_group_id) ? 'uk-active' : ''?>">
			    	<a href="<?=Url::toRoute(['show-rasp', 'group_id' => $group['id']])?>">
			    		<?=$group['faculty']?>-<?=$group['num_of_group']?>
			    	</a>
			    </li>
			<?php endforeach; ?>
		</ul>
	</div><!-- .uk-width-3-3 -->						
</div><!-- .uk-grid -->

<div class="uk-grid">
	<div class="uk-width-large-1-2">
		<h2>
			<a class="uk-button" id="dayrasp_prev" href="">
				<i class="uk-icon-arrow-left"></i>
			</a>
			<span class="uk-text-middle" id="dateText"></span>
			<a class="uk-button" id="dayrasp_next" href="">
				<i class="uk-icon-arrow-right"></i>
			</a>
		</h2>

		<a class="uk-button uk-button-small" href="">вчера</a>
		<a class="uk-button uk-button-small uk-active" href="">сегодня</a>
		<a class="uk-button uk-button-small" href="">завтра</a>

		<?php if (isset($lessons)): ?> 
			<?= $this->render('rasp_day', compact('lessons')); ?>
		<?php endif; ?>

	</div><!-- .uk-width-large-1-2 -->
	<div class="uk-width-large-1-2 uk-hidden-medium uk-hidden-small">
		<h3>Основы математического анализа</h3>
		<table class="uk-table uk-table-striped uk-table-condensed uk-table-hover">
			<caption>Контрольные мероприятия</caption>
			<tbody>
				<tr>
					<td>4</td>
					<td>Лабораторная работа </td>
					<td>3-10</td>
					<td>0</td>
					<td><small><i>изм. 30.08.16</i></small></td>
				</tr>
				<tr>
					<td>6</td>
					<td>Лабораторная работа </td>
					<td>3-10</td>
					<td>0</td>
					<td><small><i>изм. 30.08.16</i></small></td>
				</tr>
				<tr>
					<td>8</td>
					<td>Лабораторная работа </td>
					<td>3-10</td>
					<td>0</td>
					<td><small><i>изм. 30.08.16</i></small></td>
				</tr>
				<tr>
					<td>10</td>
					<td>Лабораторная работа </td>
					<td>3-10</td>
					<td>0</td>
					<td><small><i>изм. 30.08.16</i></small></td>
				</tr>
				<tr>
					<td>12</td>
					<td>Лабораторная работа </td>
					<td>3-10</td>
					<td>0</td>
					<td><small><i>изм. 30.08.16</i></small></td>
				</tr>
			</tbody>
		</table>
		<h2>Долги</h2>
		<form class="uk-form" ="">
            <div class="uk-alert uk-alert-danger" data-uk-alert="">
                <a class="uk-alert-close uk-close"></a>
                <p>КР_1 <small><i>Добавлено: 1.01.2016</i></small></p>
            </div>
            <div class="uk-alert uk-alert-danger" data-uk-alert="">
                <a class="uk-alert-close uk-close"></a>
                <p>КР_2 <small><i>Добавлено: 1.01.2016</i></small></p>
            </div>
            <div class="uk-form-row">
                <input type="text" placeholder="Описание" class="uk-form-small uk-form-width-small">
                <button class="uk-button uk-button-small" type="reset">Добавить</button>
            </div>
        </form>
	</div><!-- .uk-width-large-1-2 -->
</div><!-- .uk-grid -->

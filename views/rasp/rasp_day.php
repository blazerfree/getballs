<?php 
	use yii\helpers\VarDumper; 
?>

<div class="b-list b-list_rasps b-list_rasps_day i-rasps" id="dayrasp">
	<script type="text/template" id="dayrasp_template">
		<% lessons.forEach(function(lesson) { %>
			<div class="b-rasp i-rasp" data-date="<%= lesson.date %>">
				<div class="b-rasp-progress i-rasp-progress"></div>
				<div class="b-rasp-time b-rasp-time_begin i-rasp-beginTime">
					<%=lesson.beginTime %>
				</div>
				<div class="b-rasp-time b-rasp-time_end i-rasp-endTime">
					<%=lesson.endTime %>
				</div>
				<div class="b-rasp-timeleft i-rasp-timeleft"></div>
				<div class="uk-grid uk-grid-small">
					<div class="uk-width-small-1-1 uk-width-large-1-10 uk-text-right">
						<span class="uk-badge uk-badge-warning uk-text-small"><%= lesson.auditory %></span>
						<span class="uk-badge uk-text-small">
							<%= lesson.type_of_lesson %>
						</span>
					</div>
					<div class="uk-width-small-1-1 uk-width-large-7-10">
						<div class="uk-text-large"><%= lesson.name %></div>
						<span class=""><%= lesson.prepod %></span>
					</div>
					<div class="uk-width-small-1-1 uk-width-large-2-10 uk-text-right">
						<div>
							<small>Баллы:</small>
							<span class="uk-badge uk-badge-danger">
								?
							</span>
						</div>
						<label class="">
							<small>Посещение:</small>
							?
							<input type="checkbox">
						</label>
					</div>
				</div><!-- .uk-grid" -->
			</div>
		<% }); %>
	</script>
</div><!-- .b-list -->

<?php

	$this->registerJsFile('@web/js/renderTemplates.js', [
		'depends' => ['app\assets\RaspAsset'],
		'position' => $this::POS_END,
	]);

	$this->registerJs(''
		. 'var lessons = ' . json_encode($lessons) . ';'
		. 'var dayrasp = new DayRasp({
				lessonsJson: lessons,
				template: _.template(document.getElementById("dayrasp_template").innerHTML),
				dateTextId: "dateText",
				intoId: "dayrasp",
			}); updateRasps(); ', $this::POS_END);

	// $this->registerJsFile('@web/js/update.js', [
	// 	'depends' => ['app\assets\RaspAsset'],
	// 	'position' => $this::POS_END
	// ]);
?>
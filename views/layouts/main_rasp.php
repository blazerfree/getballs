<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\RaspAsset;

RaspAsset::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="l-wrap">
<div class="l-head">
	<div class="l-head-inner">
		<div class="uk-container uk-container-center">
			<div class="b-head">
				<nav class="uk-navbar">
					<ul class="uk-navbar-nav">
						<li class="uk-active">
							<a href="#" class="uk-navbar-nav-subtitle">
								Расписание
								<div>На день</div>
							</a>
						</li>
						<li>
							<a href="#" class="uk-navbar-nav-subtitle">
								Обзор
								<div>недели</div>
							</a>
						</li>
						<li>
							<a href="#" class="uk-navbar-nav-subtitle">
								Основное
								<div>Расписание на сегодня</div>
							</a>
						</li>
					</ul>
					<div class="uk-navbar-flip">
	                    <div class="uk-navbar-content">
	                    	<span class="uk-badge">1-ая неделя</span>
	                    </div>
	                </div>
				</nav><!-- .uk-navbar -->
			</div><!-- .b-head -->
		</div><!-- .uk-container -->
	</div><!-- .l-head-inner -->
</div><!-- .l-head -->

<div class="l-main">
	<div class="uk-container uk-container-center b-main">
		 <?= $content ?>
	</div><!-- .uk-container.b-main -->
</div><!-- .l-main -->

<div class="l-foot">
	<div class="l-foot-inner">
		<div class="b-foot">
			<br>
			<br>
			<br>
			<br>
		</div><!-- .b-foot -->
	</div><!-- .l-foot-inner -->	
</div><!-- .l-foot -->
</div><!-- .l-wrap -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

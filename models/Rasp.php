<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use app\helpers\Translit;

use app\models\Group;

// расписание конкртеной группы
class Rasp extends \yii\base\Model
{
	public $group;

	/**
	 * Импортирует аудитории из JSON в БД.
	 * @param  string $rasp_json   [description]
	 * @param  string $years       [description]
	 * @param  string $semester    [description]
	 * @return array $auditories_numbers [массив с id всех записанных групп]
	 */
	public function importAuditories($rasp_json, $years, $semester)
	{
		$rasp = Json::decode($rasp_json);
		array_pop($rasp); //убираем последний элемент с числом записей
	
		$bd_auditories = Auditory::find()->all();
		$bd_auditories_arr = ArrayHelper::toArray($bd_auditories);
		$bd_auditories_numbers = ArrayHelper::map($bd_auditories_arr, 'number', 'id');

		$auditories_numbers = [];

		foreach ($rasp as $lesson) {
			$auditory_number = $lesson['auditory'];

			if ( !ArrayHelper::keyExists($auditory_number, $bd_auditories_numbers) 
				&& !ArrayHelper::keyExists($auditory_number, $auditories_numbers) ) {

				$auditory = new Auditory();
				$auditory->number = $auditory_number;
				$auditory->save();

				$auditories_numbers[$auditory_number] = $auditory->id;
			}
		}

		return $auditories_numbers;
	}

	/**
	 * Импортирует группы из JSON в БД.
	 * @param  string $rasp_json   [description]
	 * @param  string $years       [description]
	 * @param  string $semester    [description]
	 * @return array $groups_codes [массив с id всех записанных групп]
	 */
	public function importGroups($rasp_json, $years, $semester)
	{
		$rasp = Json::decode($rasp_json);
		array_pop($rasp); //убираем последний элемент с числом записей
	
		$bd_groups = Group::find()->all();
		$bd_groups_arr = ArrayHelper::toArray($bd_groups);
		$bd_groups_codes = ArrayHelper::map($bd_groups_arr, 'group_code', 'id');

		$groups_codes = [];

		foreach ($rasp as $lesson) {
			// Формируем код группы. (Пример: 'mp_44_2016_2017_1')
			$group_code = ''
				. Translit::toLatin($lesson['group']['faculty'])
				. "_{$lesson['group']['number']}_{$years}_{$semester}";

			// Если кода группы нет в базе и нет в текущем json, то создаем эту группу 
			if ( !ArrayHelper::keyExists($group_code, $bd_groups_codes) 
				&& !ArrayHelper::keyExists($group_code, $groups_codes) ) {
				
				$group = new Group();
				$group->faculty      = Translit::toLatin($lesson['group']['faculty']);
				$group->course       = $lesson['group']['course'];
				$group->num_of_group = $lesson['group']['number'];
				$group->group_code   = $group_code;
				$group->save();

				$groups_codes[$group_code] = $group->id;
			}
		}

		return $groups_codes;
	}

	/**
	 * Импортирует предметы из JSON в БД, соотнося с группами из БД.
	 * Возвращает массив с id всех записанных предметов.
	 * @param  [type] $rasp_json   [description]
	 * @param  [type] $years       [description]
	 * @param  [type] $semester    [description]
	 * @return array $subjects_codes [массив с id всех записанных предметов]
	 */
	public function importSubjects($rasp_json, $years, $semester)
	{
		$rasp = Json::decode($rasp_json);
		array_pop($rasp); //убираем последний элемент с числом записей

		// Получаем массив с кодами группы
		$bd_groups = Group::find()->all();
		$bd_groups_arr = ArrayHelper::toArray($bd_groups);
		$bd_groups_codes = ArrayHelper::map($bd_groups_arr, 'group_code', 'id');

		$bd_subjects = Subject::find()->all();
		$bd_subjects_arr = ArrayHelper::toArray($bd_subjects);
		$bd_subjects_codes = ArrayHelper::map($bd_subjects_arr, 'subject_code', 'id');

		$subjects_codes = [];

		foreach ($rasp as $lesson) {
			// Формируем код группы. (Пример: 'mp_44_2016_2017_1')
			// Он необходим для того, чтобы далее соотнести данный lesson (из json)
			// с нужной группой
			$lesson_group_code = ''
				. Translit::toLatin($lesson['group']['faculty'])
				. "_{$lesson['group']['number']}_{$years}_{$semester}";

			$lesson_name = Translit::toLatin($lesson['name']);

			foreach ($bd_groups_codes as $bd_group_code => $bd_group_id) {
				// Формируем код предмета. (Пример: 'nejronnye_seti_mp_45_2016_2017_1')
				$subject_code = "{$lesson_name}_{$bd_group_code}";

				if ( $bd_group_code == $lesson_group_code 
					&& !ArrayHelper::keyExists($subject_code, $bd_subjects_codes) 
					&& !ArrayHelper::keyExists($subject_code, $subjects_codes) ) {

					switch ($semester) {
						case '1':
							$date_of_begin = explode('_', $years)[0] . '-09-01'; // 1 сен
							$date_of_end = explode('_', $years)[0] . '-12-31'; // 31 дек
							break;
						case '2':
							$date_of_begin = explode('_', $years)[1] . '-01-01'; // 1 янв
							$date_of_end = explode('_', $years)[1] . '-07-01'; // 1 июл
							break;
						default:
							break;
					}

					$subjectDataParams = [
						'name'          => $lesson['name'],
						'date_of_begin' => $date_of_begin,
						'date_of_end'   => $date_of_end,
						'group_id'      => $bd_group_id,
						'subject_code'  => $subject_code,
					];

					$subject = new Subject();
					$subject -> setData($subjectDataParams);
					$subject -> save();

					$subjects_codes[$subject_code] = $subject->id;
				}
			}
		}

		return $subjects_codes;
	}

	/**
	 * Импортирует мероприятия из JSON в БД, соотнося с предметами из БД.
	 * Возвращает массив с id всех записанных предметов.
	 * @param  [type] $rasp_json    [description]
	 * @param  [type] $years        [description]
	 * @param  [type] $semester     [description]
	 * @return array $lessons_codes [массив с id всех записанных мероприятий]
	 */
	public function importLessons($rasp_json, $years, $semester)
	{
		$rasp = Json::decode($rasp_json);
		array_pop($rasp); //убираем последний элемент с числом записей

		$bd_auditories = Auditory::find()->all();
		$bd_auditories_arr = ArrayHelper::toArray($bd_auditories);
		$bd_auditories_numbers = ArrayHelper::map($bd_auditories_arr, 'number', 'id');

		$bd_subjects = Subject::find()->all();
		$bd_subjects_arr = ArrayHelper::toArray($bd_subjects);
		$bd_subjects_codes = ArrayHelper::map($bd_subjects_arr, 'subject_code', 'id');

		$bd_lessons_codes = [];

		$query = (new Query)->from('lesson');
		foreach ($query->batch(1000) as $i => $bd_lessons) {
		   $bd_lessons_arr = ArrayHelper::toArray($bd_lessons);
		   $bd_lessons_codes = array_merge($bd_lessons_codes, ArrayHelper::map($bd_lessons_arr, 'lesson_code', 'id'));
		}

		$lessons_codes = [];
		$lessons_forSave = [];

		foreach ($rasp as $lesson) {
			// Формируем номер аудитории.
			$lesson_auditory_number = $lesson['auditory'];
			$lesson_period = Translit::toLatin($lesson['period']);

			// Формируем код предмета. (Пример: 'nejronnye_seti_mp_45_2016_2017_1')
			// Он необходим для того, чтобы далее соотнести данный lesson из json (мероприятие)
			// с нужным предметом в БД
			$lesson_subject_code = ''
				. Translit::toLatin($lesson['name']) . '_'
				. Translit::toLatin($lesson['group']['faculty'])
				. "_{$lesson['group']['number']}_{$years}_{$semester}";

			// Формируем левую часть кода мероприятия. 
			// Пример: 'praktika_ponedelnik_every_1_practice'
			$lesson_code_part = Translit::toLatin("{$lesson['name']}-{$lesson['day']}-{$lesson['period']}-{$lesson['pair']}-{$lesson['type']}");

			foreach ($bd_subjects_codes as $bd_subject_code => $bd_subject_id) {
				// Оконачательно формируем код мероприятия
				// Пример: 'praktika_ponedelnik_every_1_practice_5'
				$lesson_code ="{$lesson_code_part}_{$bd_subject_id}";

				if ( $bd_subject_code == $lesson_subject_code
					&& !ArrayHelper::keyExists($lesson_code, $bd_lessons_codes) 
					/* && !ArrayHelper::keyExists($lesson_code, $lessons_codes) */ ) {

					$auditory_id = NULL;
					foreach ($bd_auditories_numbers as $bd_auditory_number => $bd_auditory_id) {
						if ($bd_auditory_number == $lesson_auditory_number) {
							$auditory_id = $bd_auditory_id;
						}
					}

					array_push($lessons_forSave, [
						$lesson['type'],
						$lesson['pair'],
						1,
						$lesson['day'],
						$lesson_period,
						$lesson['auditory'],
						false,
						$bd_subject_id,
						$auditory_id,
						$lesson_code,
					]);

					$lessons_codes[$lesson_code] = '1';
				}
			}
		}

		Yii::$app->db->createCommand()->batchInsert('lesson', [
			'type_of_lesson',
			'start_pair',
			'pairs_number',
			'day',
			'period',
			'auditory',
			'is_visited',
			'subject_id',
			'auditory_id',
			'lesson_code',
		], $lessons_forSave) -> execute();

		return $lessons_codes;
	}

	public function createDatelessons()
	{
		$datelessons = []; // массив, который будет записан в БД

		$bd_subjects = [];
		$querySubjects = (new Query)->from('subject');
		foreach ($querySubjects->batch(1000) as $i => $batch) { 
			$bd_subjects = array_merge($bd_subjects,  ArrayHelper::toArray($batch));
		}
		$bd_subjects_indexed = ArrayHelper::index($bd_subjects, 'id');

		$bd_lessons = [];
		$queryLessons = (new Query)->from('lesson');
		foreach ($queryLessons->batch(1000) as $i => $batch) {
			$bd_lessons = array_merge($bd_lessons,  ArrayHelper::toArray($batch));
		}

		foreach ($bd_lessons as $bd_lesson_id => $bd_lesson) {
			$bd_lesson_subject_id = $bd_lesson['subject_id'];
			$bd_subject = $bd_subjects_indexed[$bd_lesson_subject_id];

			$bd_beginDate = new \DateTime($bd_subject['date_of_begin']);
			$bd_endDate   = new \DateTime($bd_subject['date_of_end']);

			$begin_day_pos = strftime('%u', $bd_beginDate->getTimestamp());

			// сдвинем дату на 1ый день недели
			$day_offset = $begin_day_pos - 1;
			$beginDate_monday = new \DateTime($bd_subject['date_of_begin']);
			$beginDate_monday -> modify("-{$day_offset} day");

			$init_days_offset = 0; // начальное смещение относительно 1го дня недели
			$nextAfter = 0; // кол-во дней до след.

			switch ($bd_lesson['period']) {
				case 'every':
					$init_days_offset = 0;
					$nextAfter = 7;
					break;
				case 'ch':
					$init_days_offset = 0;
					$nextAfter = 7;
					break;
				case 'zn':
					$init_days_offset = 7;
					$nextAfter = 7;
					break;
				case 'ch_i':
					$init_days_offset = 0;
					$nextAfter = 28;
					break;
				case 'z_i':
					$init_days_offset = 7;
					$nextAfter = 28;
					break;
				case 'ch_ii':
					$init_days_offset = 14;
					$nextAfter = 28;
					break;
				case 'z_ii':
					$init_days_offset = 21;
					$nextAfter = 28;
					break;
				
				default:
					break;
			}

			// задаем начальное смещение дате относительно 1го дня недели, учитывая день недели у предмета
			$init_offset = $bd_lesson['day'] + $init_days_offset - 1;
			$newDate = $beginDate_monday -> modify("+{$init_offset} day");

			while ($bd_beginDate > $newDate) {
				$newDate -> modify("+{$nextAfter} day");
			}

			while ($newDate <= $bd_endDate) {
				array_push($datelessons, [
					$bd_lesson['id'],
					$newDate -> format('Y-m-d')
				]);

				$newDate -> modify("+{$nextAfter} day");
			}
		}
		Yii::$app->db->createCommand()->batchInsert('datelesson', [
			'lesson_id',
			'date'
		], $datelessons) -> execute();

		return [];
	}

	public function getAllGroups ()
	{
		$groups = Group::find()
			->orderBy([
				'group_code' => SORT_ASC
			])->all();

		$groups_arr = ArrayHelper::toArray($groups);

		return $groups_arr;
	}

	public function getFirstGroup ()
	{
		return $group = Group::find()
			->orderBy([
				'group_code' => SORT_ASC
			])->one();
	}

	public function test ()
	{
		$bd_test = Lesson::find()
			->joinWith('group')
			->where(['group.faculty' => 'mp'])
			->andWhere(['group.num_of_group' => '49'])
			->all();

		$bd_test_arr = ArrayHelper::toArray($bd_test);

		return $bd_test_arr;
	}
}

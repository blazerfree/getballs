<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subject".
 *
 * @property integer $id
 * @property string $name
 * @property string $date_of_begin
 * @property string $date_of_end
 * @property integer $group_id
 *
 * @property Kms[] $kms
 * @property Lesson[] $lessons
 * @property Group $group
 * @property SubjectPrepod[] $subjectPrepods
 */
class Subject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subject';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id'], 'required'],
            [['group_id'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Group::className(), 'targetAttribute' => ['group_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'date_of_begin' => 'Date Of Begin',
            'date_of_end' => 'Date Of End',
            'group_id' => 'Group ID',
            'subject_code' => 'Subject Code',
        ];
    }

    public function setData($params)
    {
        $this->name             = isset($params['name'])             ? $params['name']             : null;
        $this->date_of_begin    = isset($params['date_of_begin'])    ? $params['date_of_begin']    : null;
        $this->date_of_end      = isset($params['date_of_end'])      ? $params['date_of_end']      : null;
        $this->group_id         = isset($params['group_id'])         ? $params['group_id']         : null;
        $this->subject_code     = isset($params['subject_code'])     ? $params['subject_code']     : null;

        return $this;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKms()
    {
        return $this->hasMany(Kms::className(), ['subject_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessons()
    {
        return $this->hasMany(Lesson::className(), ['subject_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjectPrepods()
    {
        return $this->hasMany(SubjectPrepod::className(), ['subject_id' => 'id']);
    }
}

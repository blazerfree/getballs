<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prepod".
 *
 * @property integer $id
 * @property string $name
 * @property string $photo
 *
 * @property SubjectPrepod[] $subjectPrepods
 */
class Prepod extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prepod';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['photo'], 'string'],
            [['name'], 'string', 'max' => 80],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'photo' => 'Photo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjectPrepods()
    {
        return $this->hasMany(SubjectPrepod::className(), ['prepod_id' => 'id']);
    }
}

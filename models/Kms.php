<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kms".
 *
 * @property integer $id
 * @property string $type
 * @property string $name
 * @property double $max_ball
 * @property double $min_ball
 * @property double $current_ball
 * @property string $write_by
 * @property integer $week
 * @property integer $subject_id
 *
 * @property Subject $subject
 */
class Kms extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['max_ball', 'min_ball', 'current_ball'], 'number'],
            [['week', 'subject_id'], 'integer'],
            [['type'], 'string', 'max' => 50],
            [['name'], 'string', 'max' => 80],
            [['write_by'], 'string', 'max' => 100],
            [['subject_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subject::className(), 'targetAttribute' => ['subject_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'name' => 'Name',
            'max_ball' => 'Max Ball',
            'min_ball' => 'Min Ball',
            'current_ball' => 'Current Ball',
            'write_by' => 'Write By',
            'week' => 'Week',
            'subject_id' => 'Subject ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['id' => 'subject_id']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lesson".
 *
 * @property integer $id
 * @property string $type_of_lesson
 * @property string $start_pair
 * @property integer $pairs_number
 * @property integer $day
 * @property integer $period
 * @property string $auditory
 * @property integer $is_visited
 * @property integer $subject_id
 * @property integer $auditory_id
 * @property string $lesson_code
 *
 * @property Subject $subject
 * @property Auditory $auditory0
 */
class Lesson extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lesson';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_pair', 'pairs_number', 'is_visited', 'subject_id', 'auditory_id'], 'integer'],
            [['subject_id'], 'required'],
            [['type_of_lesson'], 'string', 'max' => 30],
            [['auditory'], 'string', 'max' => 10],
            [['subject_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subject::className(), 'targetAttribute' => ['subject_id' => 'id']],
            [['auditory_id'], 'exist', 'skipOnError' => true, 'targetClass' => Auditory::className(), 'targetAttribute' => ['auditory_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_of_lesson' => 'Type Of Lesson',
            'start_pair' => 'Start Pair',
            'pairs_number' => 'Pairs Number',
            'day' => 'Day',
            'period' => 'Period',
            'auditory' => 'Auditory',
            'is_visited' => 'Is Visited',
            'subject_id' => 'Subject ID',
            'auditory_id' => 'Auditory ID',
            'lesson_code' => 'Lesson Code',
        ];
    }

    public function setData($params)
    {
        $this->type_of_lesson = isset($params['type_of_lesson']) ? $params['type_of_lesson'] : null;
        $this->start_pair     = isset($params['start_pair'])     ? $params['start_pair']     : null;
        $this->pairs_number   = isset($params['pairs_number'])   ? $params['pairs_number']   : null;
        $this->day            = isset($params['day'])            ? $params['day']            : null;
        $this->period         = isset($params['period'])         ? $params['period']         : null;
        $this->auditory       = isset($params['auditory'])       ? $params['auditory']       : null;
        $this->is_visited     = isset($params['is_visited'])     ? $params['is_visited']     : null;
        $this->subject_id     = isset($params['subject_id'])     ? $params['subject_id']     : null;
        $this->auditory_id    = isset($params['auditory_id'])    ? $params['auditory_id']    : null;
        $this->lesson_code    = isset($params['lesson_code'])    ? $params['lesson_code']    : null;

        return $this;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['id' => 'subject_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this
            ->hasOne(Group::className(), ['id' => 'group_id'])
            ->viaTable(Subject::tableName(), ['id' => 'subject_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuditory0()
    {
        return $this->hasOne(Auditory::className(), ['id' => 'auditory_id']);
    }
}

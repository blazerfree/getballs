<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "group".
 *
 * @property integer $id
 * @property string $faculty
 * @property integer $course
 * @property integer $num_of_group
 * @property string $group_code
 *
 * @property Subject[] $subjects
 */
class Group extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'group';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['course', 'num_of_group'], 'integer'],
			[['faculty'], 'string', 'max' => 10],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'faculty' => 'Faculty',
			'course' => 'Course',
			'num_of_group' => 'Num Of Group',
			'group_code' => 'Code of Group',
		];
	}

	public function addGroup($params)
	{
		// $this->name = 
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSubjects()
	{
		return $this->hasMany(Subject::className(), ['group_id' => 'id']);
	}

	public function getDayRasp ()
	{
		/*
		$lessons = [];
		$subjects = $this->subjects;
		foreach ($subjects as $key => $subject) {
			array_push($lessons, $subject->lessons);
		}
		*/
	   
		$datelessons = [];
		$array_pairs = require(\Yii::$app->basePath . '/data/array_pairs.php');

		$bd_lessons = [];
		$queryLessons = (new Query)->from('lesson')
			->leftJoin('subject', 'subject.id = lesson.subject_id')
			->leftJoin('group', 'group.id = subject.group_id')
			->where(['group.id' => $this->id])
			->select([
				'type_of_lesson',
				'start_pair',
				'pairs_number',
				'day',
				'period',
				'auditory',
				'name',
				'date_of_begin',
				'date_of_end',
			]);
			
		foreach ($queryLessons->batch(1000) as $i => $batch) {
			$bd_lessons = array_merge($bd_lessons,  ArrayHelper::toArray($batch));
		}       

		// return ArrayHelper::toArray($bd_lessons);

		foreach ($bd_lessons as $bd_lesson_id => $bd_lesson) {
			$bd_beginDate = new \DateTime($bd_lesson['date_of_begin']);
			$bd_endDate   = new \DateTime($bd_lesson['date_of_end']);

			$begin_day_pos = strftime('%u', $bd_beginDate->getTimestamp());

			// сдвинем дату на 1ый день недели
			$day_offset = $begin_day_pos - 1;
			$beginDate_monday = new \DateTime($bd_lesson['date_of_begin']);
			$beginDate_monday -> modify("-{$day_offset} day");

			$init_days_offset = 0; // начальное смещение относительно 1го дня недели
			$nextAfter = 0; // кол-во дней до след.

			switch ($bd_lesson['period']) {
				case 'every':
					$init_days_offset = 0;
					$nextAfter = 7;
					break;
				case 'ch':
					$init_days_offset = 0;
					$nextAfter = 14;
					break;
				case 'zn':
					$init_days_offset = 7;
					$nextAfter = 14;
					break;
				case 'ch_i':
					$init_days_offset = 0;
					$nextAfter = 28;
					break;
				case 'z_i':
					$init_days_offset = 7;
					$nextAfter = 28;
					break;
				case 'ch_ii':
					$init_days_offset = 14;
					$nextAfter = 28;
					break;
				case 'z_ii':
					$init_days_offset = 21;
					$nextAfter = 28;
					break;
				
				default:
					break;
			}

			// задаем начальное смещение дате относительно 1го дня недели, учитывая день недели у предмета
			$init_offset = $bd_lesson['day'] + $init_days_offset - 1;
			$newDate = $beginDate_monday -> modify("+{$init_offset} day");

			while ($bd_beginDate > $newDate) {
				$newDate -> modify("+{$nextAfter} day");
			}

			while ($newDate <= $bd_endDate) {
				$date_string = "{$newDate -> format('Y-m-d')}";

				$start_pair     = $bd_lesson['start_pair'];
				$beginTime      = $array_pairs[$start_pair]['begin'];
				$endTime        = $array_pairs[$start_pair]['end'];
				$auditory       = $bd_lesson['auditory'];
				$type_of_lesson = $bd_lesson['type_of_lesson'];

				switch ($type_of_lesson) {
					case '[practice]':
						$type_of_lesson = "пр.";
						break;
					case '[Пр]':
						$type_of_lesson = "сем.";
						break;
					case '[Лек]':
						$type_of_lesson = "лек.";
						break;
					case '[Лаб]':
						$type_of_lesson = "лаб.";
						break;
					default:
						$type_of_lesson = null;
						break;
				}

				if (!ArrayHelper::keyExists($date_string, $datelessons)) {
					$datelessons[$date_string] = [];
				}

				array_push($datelessons[$date_string], [
					'type_of_lesson' => $type_of_lesson,
					'start_pair'     => $start_pair,
					'pairs_number'   => $bd_lesson['pairs_number'],
					'day'            => $bd_lesson['day'],
					'period'         => $bd_lesson['period'],
					'auditory'       => $auditory,
					'name'           => $bd_lesson['name'],
					'date'           => $date_string,
					'beginTime'      => $beginTime,
					'endTime'        => $endTime,
				]);

				ArrayHelper::multisort($datelessons[$date_string], 'start_pair');


				$newDate -> modify("+{$nextAfter} day");
			}
		}

		return $datelessons;
	}
}

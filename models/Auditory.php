<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "auditory".
 *
 * @property integer $id
 * @property string $number
 *
 * @property Lesson[] $lessons
 */
class Auditory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auditory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Number',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessons()
    {
        return $this->hasMany(Lesson::className(), ['auditory_id' => 'id']);
    }
}

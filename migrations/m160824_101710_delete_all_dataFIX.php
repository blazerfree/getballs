<?php

use yii\db\Migration;

class m160824_101710_delete_all_dataFIX extends Migration
{
    public function up()
    {
        // Yii::$app->db->createCommand()->truncateTable('auditory')->execute();
        // Yii::$app->db->createCommand()->truncateTable('country')->execute();
        // 
        Yii::$app->db->createCommand()->execute("SET foreign_key_checks = 0");
        // Yii::$app->db->createCommand()->truncateTable('group')->execute();
        Yii::$app->db->createCommand()->delete('auditory')->execute();
        // Yii::$app->db->createCommand()->truncateTable('lesson')->execute();
        // Yii::$app->db->createCommand()->truncateTable('lesson_auditory')->execute();
        // Yii::$app->db->createCommand()->truncateTable('prepod')->execute();
        // Yii::$app->db->createCommand()->truncateTable('subject')->execute();
        // Yii::$app->db->createCommand()->truncateTable('subject_prepod')->execute();  
        Yii::$app->db->createCommand()->execute("SET foreign_key_checks = 1");     
    }

    public function down()
    {
        echo "m160824_101710_delete_all_dataFIX cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

<?php

use yii\db\Migration;

class m160824_100013_create_new_column_in_groupFIX extends Migration
{
    public function up()
    {
        Yii::$app->db->createCommand()->addColumn('group', 'group_code', 'string(30)')->execute();
    }

    public function down()
    {
        echo "m160824_100013_create_new_column_in_groupFIX cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

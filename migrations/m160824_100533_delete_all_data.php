<?php

use yii\db\Migration;

class m160824_100533_delete_all_data extends Migration
{
    public function up()
    {
        Yii::$app->db->createCommand()
            ->truncateTable('auditory')
            ->truncateTable('country')
            ->truncateTable('group')
            ->truncateTable('kms')
            ->truncateTable('lesson')
            ->truncateTable('lesson_auditory')
            ->truncateTable('prepod')
            ->truncateTable('subject')
            ->truncateTable('subject_prepod')
            ->execute();
    }

    public function down()
    {
        echo "m160824_100533_delete_all_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}

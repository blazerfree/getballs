var currentTime;
var this_date;
var allrasps = [];
moment.locale('ru');

$.fn.rasp = function (options) {
	this.hasPassed = false; // Прошло ли событие
	this.progress = 0;

	options = options || {};
	options.beginDateTime = options.beginDateTime;
	options.endDateTime = options.endDateTime;
	
	var beginDateTime = moment(options.beginDateTime, 'DD.MM.YYYY HH:mm');
	var endDateTime = moment(options.endDateTime, 'DD.MM.YYYY HH:mm');

	this.beginDateTime = beginDateTime;
	this.endDateTime = endDateTime;

	var $this = $(this);
	var diffTimes = endDateTime.diff(beginDateTime);

	var progressBar = $this.find('.i-rasp-progress');
	var progressBarBackground = '#faa732';

	this.update = function () {
		if (!this.hasPassed) {
			this.progress = 1 * moment().diff(beginDateTime) / diffTimes;

			if (this.progress >= 1) {
				progressBarBackground = '#82bb42';
				this.hasPassed = true;
				this.progress = 1;
				$this.addClass('m-passed')
			}

			progressBar.css({
				height: this.progress * 100 + '%',
				background: progressBarBackground
			});

			return this.progress;
		}
	}
	return this;
}



function DayRasp (options) {

	options = options || {};
	options.intoId = options.intoId || "";
	options.nextBtnId = options.nextBtnId || options.intoId + "_next";
	options.prevBtnId = options.prevBtnId || options.intoId + "_prev";

	var lessons = JSON.parse(options.lessonsJson);
	var dateText = $("#"+options.dateTextId);
	var resultHtml = $("#"+options.intoId);
	var dateNow = moment();

	function init () {
		this_date = dateNow;
		updateDayLessons(dateNow);
	};

	function updateTechBlocks () {
		dateText.html('' + 
			this_date.format("DD MMM") +
			" (" + this_date.format("ddd") + ")"
		);
		resultHtml.data("date", dateNow);

	};

	function updateDayLessons (date) {
		var dayLessons = lessons[date.format("YYYY-MM-DD")];

		if (dayLessons) {
			var result = options.template({
				lessons: dayLessons
			});
			
			resultHtml.html(result);
		} else {
			resultHtml.html("<h3 style='text-align: center;'>Свободный день " +
				date.format("DD.MM.YY") +
				" (" + date.format("dddd") + ")" +
				"</h3>");
		}

		updateTechBlocks();
		updateRasps();
	}

	$("#"+options.prevBtnId).click(function(event) {
		event.preventDefault();

		this_date = moment(this_date).add(-1, 'days');
		updateDayLessons(this_date);
	});

	$("#"+options.nextBtnId).click(function(event) {
		event.preventDefault();

		this_date = moment(this_date).add(1, 'days');
		updateDayLessons(this_date);
	});

	init();
}

function updateTime () {
   	currentTime = moment();

   	allrasps.forEach(function (item) {
   		item.update();
   	});
};

var interval;

function updateRasps () {
	clearInterval(interval);
	allrasps = [];

	$('.i-rasps .i-rasp').each(function() {
		var rasp = $(this);

		var thisRasp = rasp.rasp({
			beginDateTime: this_date.format("DD.MM.YYYY ") + rasp.find('.i-rasp-beginTime').text().trim(),
			endDateTime: this_date.format("DD.MM.YYYY ") + rasp.find('.i-rasp-endTime').text().trim(),
		});

		allrasps.push(thisRasp);	
	});

	updateTime();
	interval = setInterval(updateTime, 1000);
}
<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Json;

use app\models\Rasp;
use app\models\Group;

class RaspController extends Controller
{
	public $layout = '@app/views/layouts/main_rasp.php';

	public $defaultAction = 'showRasp';

	public function actionIndex()
	{
		
	}

	public function actionShowRasp($group_id = null)
	{
		$rasp = new Rasp();
		$groups = $rasp->getAllGroups();

		if (is_null($group_id)) $group_id = $rasp->getFirstGroup()->id;

		$lessons = (new Group())->findOne($group_id)->getDayRasp();

		return $this->render('index', [
			'groups' => $groups,
			'lessons' => Json::encode($lessons),
			'this_group_id' => $group_id
		]);
	}
}